# S76Stargazers

## Description
This application does 3 things:
  * Add a new repository to watch with an empty POST to /githubRepositories/:user/:repository.  It will allow duplicate users and repositories, but it will not allow a duplicate user/repository (entire GitHub url).  It will also validate the repository is real before inserting.  There is a rate limit issue here where I can only request 60 urls at a time.  This can be fixed by caching results or adding a rate limit to the endpoint but... that would be annoying if every POST after the first 60 had to wait the rest of the hour for it's turn.  Ideally, this would be an authenticated app with a better rate limit.
  * A mix task (`mix load_stargazers`) that will load all the stargazers (up to 400 due to rate limiting) for each repository in the database.  To do this daily, I would either set up a cron job that ran at a specific time, or use [crontab](https://hexdocs.pm/crontab/getting-started.html).  The benefit of crontab is there could be a different time set up for each repository to accommodate rate limits.
  * An endpoint to find all previously loaded stargazers between two dates.  If a user is a stargazer on multiple days, it only returns the user once.  There is an POST endpoint at /githubRepositories/:user/:repository/stargazers/ that takes the following input
    `{
    "start_date": "2020-01-03",
    "end_date": "2020-01-05"
    }`
    The data in priv/repo/seeds.exs has some test data you can use (all in the beginning of 2020 because I forgot what year it is).
    
##Setup
After cloning the repo:
  1. update config/dev.exs and config/test.exs with a postgres database connection.
  2. Run `mix deps.get`
  3. Eat a well-balanced meal.
  4. Run `mix ecto.migrate` to get the database created.
  5. Run `mix run priv/repo/seeds.exs` to seed the database with fake stargazer data.
  6. If you set up a test database you can also run `mix test`
  7. Run `mix phx.server` to start the app on port 4000.
  8. Once it is running you can add a new repository by modifying and running repos.http with the correct server url, user, and repo name. **
  9. You can then test the /stargazers endpoint by modifying and running stars.http with the correct server url, user, repo name, start, and end date.
  10. The mix task `mix load_stargazers` can be used to load all the stargazers (up to 400) for the day for each repo in the database. **

** - This will query GitHub multiple times and there is a rate limit of 60 calls an hour.

##Troubleshooting
If the database gets out of control rerunning `mix run priv/repo/seeds.exs` will clear it out before inserting the seed data.

To see your remaining rate limit in GitHub, make a GET to https://api.github.com/rate_limit.  It will tell you when the limit refreshes.

