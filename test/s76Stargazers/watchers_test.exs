defmodule S76Stargazers.WatchersTest do
  use S76Stargazers.DataCase

  alias S76Stargazers.Watchers

  setup do
    #delete all test stargazers before each test run
    S76Stargazers.Repo.delete_all(S76Stargazers.Watchers.Stargazer)
    :ok
  end

  describe "github_repositories" do
    alias S76Stargazers.Watchers.GithubRepository

    @valid_attrs %{user: "ocornut", repository: "imgui"}
    @repeat_user %{user: "ocornut", repository: "str"}
    @repeat_repo %{user: "b3sigma", repository: "imgui"}
    @missing_repository %{user: "ocornut", repository: nil}
    @missing_name %{user: nil, repository: "imgui"}
    @missing_all %{user: nil, repository: nil}
    @bad_repo %{user: "23523", repository: "235325235"}

    def github_repository_fixture(attrs \\ %{}) do
      {:ok, github_repository} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Watchers.create_github_repository()

      github_repository
    end

    test "get_github_repository!/1 returns the github_repository with given id" do
      github_repository = github_repository_fixture()
      assert Watchers.get_github_repository!(github_repository.id) == github_repository
    end

    test "create_github_repository/1 with valid data creates a github_repository" do
      assert {:ok, %GithubRepository{} = github_repository} = Watchers.create_github_repository(@valid_attrs)
      assert github_repository.user == "ocornut"
      assert github_repository.repository == "imgui"
    end

    test "create_github_repository/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Watchers.create_github_repository(@missing_repository)
      assert {:error, %Ecto.Changeset{}} = Watchers.create_github_repository(@missing_name)
      assert {:error, %Ecto.Changeset{}} = Watchers.create_github_repository(@missing_all)
      assert {:error, %Ecto.Changeset{}} = Watchers.create_github_repository(@bad_repo)
    end

    test "create_github_repository/1 with duplicate data returns error changeset" do
      assert {:ok, _} = Watchers.create_github_repository(@valid_attrs)
      assert {:error, %Ecto.Changeset{}} = Watchers.create_github_repository(@valid_attrs)
    end

    test "create_github_repository/1 with with duplicate user but new repo is OK" do
      assert {:ok, _} = Watchers.create_github_repository(@valid_attrs)
      assert {:ok, _} = Watchers.create_github_repository(@repeat_user)
    end

    test "create_github_repository/1 with with duplicate repo but new user is OK" do
      assert {:ok, _} = Watchers.create_github_repository(@valid_attrs)
      assert {:ok, _} = Watchers.create_github_repository(@repeat_repo)
    end

  end

  describe "stargazers" do
    alias S76Stargazers.Watchers.Stargazer

    def stargazer_fixture(attrs \\ %{}) do
      {:ok, stargazer} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Watchers.create_stargazer()

      stargazer
    end

    # This doesn't test much other than no exceptions are raised when running
    test "add daily stargazers" do
      assert :ok = Watchers.add_daily_stargazers()
    end
  end
end
