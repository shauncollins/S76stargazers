defmodule S76StargazersWeb.GithubRepositoryControllerTest do
  use S76StargazersWeb.ConnCase

  @create_attrs %{
    user: "ocornut",
    repository: "imgui"
  }

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "create github_repository" do
    test "renders github_repository when data is valid", %{conn: conn} do
      conn = post(conn, Routes.github_repository_path(conn, :create, "ocornut", "imgui"), github_repository: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.github_repository_path(conn, :show, "ocornut", "imgui", id))

      assert %{
               "id" => ^id,
               "user" => "ocornut",
               "repository" => "imgui"
             } = json_response(conn, 200)["data"]
    end

  end
end
