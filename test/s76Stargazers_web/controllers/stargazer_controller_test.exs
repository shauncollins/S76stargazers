defmodule S76StargazersWeb.StargazerControllerTest do
  use S76StargazersWeb.ConnCase

  alias S76Stargazers.Watchers
  alias S76Stargazers.Watchers.Stargazer

  def fixture(:stargazer) do
    {:ok, stargazer} = Watchers.create_stargazer(@create_attrs)
    stargazer
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  defp create_stargazer(_) do
    stargazer = fixture(:stargazer)
    %{stargazer: stargazer}
  end
end
