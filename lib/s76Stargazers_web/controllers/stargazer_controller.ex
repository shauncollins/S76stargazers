defmodule S76StargazersWeb.StargazerController do
  use S76StargazersWeb, :controller

  alias S76Stargazers.Watchers

  action_fallback S76StargazersWeb.FallbackController

  @doc """
  Query a list of stargazers for a repository over a period of time.  The request is expected to have the following
  parameters:
  user -> The user the repo belongs to
  repository -> The repository name
  start_date -> The first day to check for stargazers
  end_date -> The last day to check for stargazers
  """
  def query(conn, params) do
    with {:ok, user} <- Map.fetch(params, "user"),
         {:ok, repository} <- Map.fetch(params, "repository"),
         {:ok, start_date} <- Map.fetch(params, "start_date"),
         {:ok, end_date} <- Map.fetch(params, "end_date"),
         {:ok, github_repository} <- Watchers.get_github_repository(user, repository),
         {:ok, start_date_converted} <- Date.from_iso8601(start_date),
         {:ok, end_date_converted} <- Date.from_iso8601(end_date) do
      stargazers = Watchers.find_stargazers_between_dates(github_repository, start_date_converted, end_date_converted)
      render(conn, "index.json", stargazers: stargazers)
    end
  end

end
