defmodule S76StargazersWeb.GithubRepositoryController do
  use S76StargazersWeb, :controller

  alias S76Stargazers.Watchers
  alias S76Stargazers.Watchers.GithubRepository

  action_fallback S76StargazersWeb.FallbackController

  @doc """
  Unused for now
  """
  def index(conn, _params) do
    github_repositories = Watchers.list_github_repositories()
    render(conn, "index.json", github_repositories: github_repositories)
  end

  @doc """
  Creates a repository to monitor stargazers
  """
  def create(conn, github_repository_params) do
    with {:ok, %GithubRepository{} = github_repository} <- Watchers.create_github_repository(github_repository_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.github_repository_path(conn, :show, github_repository.user, github_repository.repository, github_repository.id))
      |> render("show.json", github_repository: github_repository)
    end
  end

  @doc """
  Returns the view of a single repository
  """
  def show(conn, %{"id" => id}) do
    github_repository = Watchers.get_github_repository!(id)
    render(conn, "show.json", github_repository: github_repository)
  end

end
