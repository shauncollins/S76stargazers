defmodule S76StargazersWeb.StargazerView do
  use S76StargazersWeb, :view
  alias S76StargazersWeb.StargazerView

  def render("index.json", %{stargazers: stargazers}) do
    %{data: render_many(stargazers, StargazerView, "stargazer.json")}
  end

  @doc """
  Because stargazers in :query are a list of names, this only returns the item
  """
  def render("stargazer.json", %{stargazer: stargazer}) do
    stargazer
  end
end
