defmodule S76StargazersWeb.GithubRepositoryView do
  use S76StargazersWeb, :view
  alias S76StargazersWeb.GithubRepositoryView

  def render("show.json", %{github_repository: github_repository}) do
    %{data: render_one(github_repository, GithubRepositoryView, "github_repository.json")}
  end

  @doc """
  Renders a single repository
  """
  def render("github_repository.json", %{github_repository: github_repository}) do
    %{id: github_repository.id,
      user: github_repository.user,
      repository: github_repository.repository}
  end
end
