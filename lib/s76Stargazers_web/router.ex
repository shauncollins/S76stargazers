defmodule S76StargazersWeb.Router do
  use S76StargazersWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", S76StargazersWeb do
    pipe_through :api
  end

  resources "/githubRepositories/:user/:repository", S76StargazersWeb.GithubRepositoryController, only: [:show, :create]

  # An endpoint to query stargazers for a repo
  post "/githubRepositories/:user/:repository/stargazers", S76StargazersWeb.StargazerController, :query

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: S76StargazersWeb.Telemetry
    end
  end
end
