defmodule Mix.Tasks.LoadStargazers do
  use Mix.Task
  @moduledoc """
  Daily load of the stargazers into the database
  """

  @shortdoc "Daily load of the stargazers into the database"

  @impl Mix.Task
  #Start the whole application
  @requirements ["app.start"]
  def run(_) do
    S76Stargazers.Watchers.add_daily_stargazers()
  end


end
