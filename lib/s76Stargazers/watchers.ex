defmodule S76Stargazers.Watchers do
  @moduledoc """
  The Watchers context.
  """

  import Ecto.Query, warn: false
  alias S76Stargazers.Repo

  alias S76Stargazers.Watchers.GithubRepository
  alias S76Stargazers.Watchers.Stargazer

  @doc """
  Returns the list of github_repositories.

  ## Examples

      iex> list_github_repositories()
      [%GithubRepository{}, ...]

  """
  def list_github_repositories do
    Repo.all(GithubRepository)
  end

  @doc """
  Gets a single github_repository.

  Raises `Ecto.NoResultsError` if the Github repository does not exist.

  ## Examples

      iex> get_github_repository!(123)
      %GithubRepository{}

      iex> get_github_repository!(456)
      ** (Ecto.NoResultsError)

  """
  def get_github_repository!(id), do: Repo.get!(GithubRepository, id)

  @doc """
  Gets a single github_repository based on the user name and repository name.

  Raises `Ecto.NoResultsError` if the Github repository does not exist.

  """
  def get_github_repository!(user, repository), do: Repo.get_by!(GithubRepository, [user: user, repository: repository])

  @doc """
  Gets a single github_repository based on the user name and repository name.
  """
  def get_github_repository(user, repository), do: {:ok, Repo.get_by(GithubRepository, [user: user, repository: repository])}

  @doc """
  Creates a github_repository.

  ## Examples

      iex> create_github_repository(%{field: value})
      {:ok, %GithubRepository{}}

      iex> create_github_repository(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_github_repository(attrs \\ %{}) do
    %GithubRepository{}
    |> GithubRepository.changeset(attrs)
    |> Repo.insert()
  end


  @doc """
  Creates a stargazer.

  ## Examples

      iex> create_stargazer(%{field: value})
      {:ok, %Stargazer{}}

      iex> create_stargazer(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_stargazer(attrs \\ %{}) do
    %Stargazer{}
    |> Stargazer.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Adds all the daily stargazers for all repositories to the database
  """
  def add_daily_stargazers() do
    list_github_repositories()
    |> Enum.each(fn(i) -> add_daily_stargazers(i) end)
  end

  @doc """
  Adds all the daily stargazers for a repository to the database
  """
  def add_daily_stargazers(%GithubRepository{} = github_repository) do
    date = Date.utc_today()
    S76Stargazers.API.GithubAPI.get_stargazers(github_repository.user, github_repository.repository)
    |> Enum.each( fn(i) -> create_stargazer(%{github_repository_id: github_repository.id, watch_date: date, stargazer: i}) end)

  end

  @doc """
  Given a github repository, find all the stargazers between the start and end date
  """
  def find_stargazers_between_dates(%GithubRepository{} = github_repository, start_date, end_date) do
    query = from s in Stargazer,
      where: s.github_repository_id == ^github_repository.id,
      where: fragment("? BETWEEN ? and ?", s.watch_date, ^start_date , ^end_date)

    Repo.all(query)
    # Only return the stargazer
    |> Enum.map( fn(i) -> i.stargazer end)
    # Only list each user once in the return
    |> Enum.uniq()
  end

end
