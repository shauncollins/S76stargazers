defmodule S76Stargazers.Watchers.Stargazer do
  use Ecto.Schema
  import Ecto.Changeset

  schema "stargazers" do
    field :stargazer, :string
    field :watch_date, :date
    field :github_repository_id, :id

    timestamps()
  end

  @doc false
  def changeset(stargazer, attrs) do
    stargazer
    |> cast(attrs, [:stargazer, :watch_date, :github_repository_id])
    |> validate_required([:stargazer, :watch_date, :github_repository_id])
    |> unique_constraint([:stargazer, :watch_date, :github_repository_id])
  end
end
