defmodule S76Stargazers.Watchers.GithubRepository do
  use Ecto.Schema
  import Ecto.Changeset

  schema "github_repositories" do
    field :user, :string
    field :repository, :string

    timestamps()
  end

  @doc """
  Validate the changeset by making sure the user and repo are populated, not already in the DB, and finally that it is
  a valid github repository
  """
  def changeset(github_repository, attrs) do
    github_repository
    |> cast(attrs, [:user, :repository])
    |> validate_required([:user, :repository])
    |> unique_constraint([:user, :repository])
    |> validate_valid_repo()
  end

  @doc """
  Check to see if the repository is a valid github repository and add an error if it is not
  """
  def validate_valid_repo(%Ecto.Changeset{valid?: false} = changeset) do
    #Do not waste any time validating the github url if the changeset is not valid
    changeset
  end

  def validate_valid_repo(%Ecto.Changeset{changes: %{user: user, repository: repository}} = changeset) do
    %{errors: errors, changes: changes} = changeset
    case S76Stargazers.API.GithubAPI.is_valid_github_link(user, repository) do
      true -> changeset
      false -> %{changeset | changes: changes, errors: [ "Invalid Github Repository URL" |  errors], valid?: false}
    end
  end

end
