defmodule S76Stargazers.API.GithubAPI do
  @moduledoc """
  An interface to the Github API using Finch.

  TODO: Rate limiting (60 requests per hour for unauthenticated users)
  """

  @doc """
  Returns true if this is a valid github repository.
  """
  def is_valid_github_link(user, repository) when is_binary(user) and is_binary(repository) do
    case get_repository(user, repository) do
      {:ok, %Finch.Response{status: 200}} -> true
      _ -> false
    end

  end

  @doc """
  Returns all the stargazers for a repository.  This data is paged in github, so it requires recursion to get all the data
  """
  def get_stargazers(user, repository) when is_binary(user) and is_binary(repository) do
    #Because this is only called from "valid" repository we are fairly confident this will return 200 (unless it's a rate limit issue)
    get_stargazers_paged(user, repository, 1, [])
  end

  # Stop at 4 pages for this due to rate limiting (page starts at 1 so the guard checks 5)
  defp get_stargazers_paged(_user, _repository, 5, current_stargazer_list) do
    current_stargazer_list
  end

  # Append the next page of stargazers to the list
  defp get_stargazers_paged(user, repository, page, current_stargazer_list) do
    #Because this is only called from "valid" repository we are fairly confident this will return 200 (unless it's a rate limit issue)
    {:ok, %Finch.Response{status: 200, body: body}} = Finch.build(:get, "#{repository_url(user, repository)}/stargazers?per_page=100&page=#{page}")
                                                      |> Finch.request(S76Finch)

    # return the usernames for each stargazer
    user_list = body
                |> Jason.decode!()
                |> Enum.map(fn(i) -> Map.fetch!(i, "login") end)

    current_stargazer_list = current_stargazer_list ++ user_list

    case Enum.count(user_list) do
      100 -> get_stargazers_paged(user, repository, page + 1, current_stargazer_list)
      _ -> current_stargazer_list
    end
  end

  # Gets the repository data from github
  defp get_repository(user, repository) when is_binary(user) and is_binary(repository) do
    Finch.build(:get, repository_url(user, repository)) |> Finch.request(S76Finch)
  end

  # Helper function to keep the github repository url in one place
  defp repository_url(user, repository) do
    "https://api.github.com/repos/#{user}/#{repository}"
  end
end
