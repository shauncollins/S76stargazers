defmodule S76Stargazers.Repo do
  use Ecto.Repo,
    otp_app: :s76Stargazers,
    adapter: Ecto.Adapters.Postgres
end
