defmodule S76Stargazers.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      S76Stargazers.Repo,
      # Start the Telemetry supervisor
      S76StargazersWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: S76Stargazers.PubSub},
      # Start the Endpoint (http/https)
      S76StargazersWeb.Endpoint,
      # Start Finch
      {Finch, name: S76Finch}
      # Start a worker by calling: S76Stargazers.Worker.start_link(arg)
      # {S76Stargazers.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: S76Stargazers.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    S76StargazersWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
