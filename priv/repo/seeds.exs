# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     S76Stargazers.Repo.insert!(%S76Stargazers.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

#Delete any existing data
S76Stargazers.Repo.delete_all(S76Stargazers.Watchers.Stargazer)
S76Stargazers.Repo.delete_all(S76Stargazers.Watchers.GithubRepository)


#Create 2 repositories to use
repo1 = S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.GithubRepository{ user: "spring-projects", repository: "spring-framework"})
# nobody watches repo 2, sad
repo2 = S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.GithubRepository{ user: "spring-projects", repository: "spring-boot"})

#Create a variety of watch dates for each repo with users

#User 1 watches repo 1  all week long
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user1", watch_date: ~D[2020-01-01]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user1", watch_date: ~D[2020-01-02]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user1", watch_date: ~D[2020-01-03]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user1", watch_date: ~D[2020-01-04]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user1", watch_date: ~D[2020-01-05]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user1", watch_date: ~D[2020-01-06]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user1", watch_date: ~D[2020-01-07]})

#User 3 watches repo 1 only the first 3 days
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user3", watch_date: ~D[2020-01-01]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user3", watch_date: ~D[2020-01-02]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user3", watch_date: ~D[2020-01-03]})

#User 4 watches repo 1 only the first 3 days
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user4", watch_date: ~D[2020-01-01]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user4", watch_date: ~D[2020-01-02]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user4", watch_date: ~D[2020-01-03]})

#User 5 watches repo 1 only the last 3 days
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user5", watch_date: ~D[2020-01-05]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user5", watch_date: ~D[2020-01-06]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user5", watch_date: ~D[2020-01-07]})

#User 6 watches repo 1 only the middle 3 days
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user6", watch_date: ~D[2020-01-03]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user6", watch_date: ~D[2020-01-04]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user6", watch_date: ~D[2020-01-05]})

#User 7 watches repo 1 only the first and last days
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user7", watch_date: ~D[2020-01-01]})
S76Stargazers.Repo.insert!(%S76Stargazers.Watchers.Stargazer{ github_repository_id: repo1.id, stargazer: "user7", watch_date: ~D[2020-01-07]})