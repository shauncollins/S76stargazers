defmodule S76Stargazers.Repo.Migrations.CreateGithubRepositories do
  use Ecto.Migration

  def change do
    create table(:github_repositories) do
      add :name, :string

      timestamps()
    end

  end
end
