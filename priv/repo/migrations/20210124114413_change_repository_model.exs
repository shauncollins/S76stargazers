defmodule S76Stargazers.Repo.Migrations.ChangeRepositoryModel do
  use Ecto.Migration

  def change do
    alter table(:github_repositories) do
      add :user, :string, null: false
      add :repository, :string, null: false
      remove :name
    end
    create unique_index(:github_repositories, [:user, :repository])
  end
end
