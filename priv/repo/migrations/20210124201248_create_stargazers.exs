defmodule S76Stargazers.Repo.Migrations.CreateStargazers do
  use Ecto.Migration

  def change do
    create table(:stargazers) do
      add :stargazer, :string, null: false
      add :watch_date, :date, null: false
      add :github_repository_id, references(:github_repositories, on_delete: :nothing), null: false

      timestamps()
    end

    create unique_index(:stargazers, [:stargazer, :github_repository_id, :watch_date])
  end
end
